from setuptools import setup, find_packages

with open('README.rst', encoding='UTF-8') as f:
    readme = f.read()

setup(
    name='FizzBuzz',
    version='0.1.0',
    description='describe',
    long_description=readme,
    author='name',
    author_email='email',
    install_requires=[],
    packages=find_packages('src'),
    package_dir={'': 'src'}
)
