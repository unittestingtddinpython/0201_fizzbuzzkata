def fizzbuzz(in_var):
    if in_var % 3 == 0 and in_var % 5 != 0:
        return "Fizz"
    elif in_var % 5 == 0 and in_var % 3 != 0:
        return "Buzz"
    elif in_var % 3 == 0 and in_var % 5 == 0:
        return "FizzBuzz"
    else:
        return str(in_var)
