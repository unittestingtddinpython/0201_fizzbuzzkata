.PHONY: install test

default: test

install:
	PYTHONPATH="$(pwd)" pipenv install --dev --skip-lock

test:
	PYTHONPATH=./src pytest
