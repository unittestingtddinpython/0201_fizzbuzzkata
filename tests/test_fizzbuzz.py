from FizzBuzz import fizzBuzz


def check_fizzbuzz(in_var, expected_result):
    result = fizzBuzz.fizzbuzz(in_var)
    assert result == expected_result


def test_return_1_with_1_passed_in():
    check_fizzbuzz(1, "1")


def test_return_2_with_2_passed_in():
    check_fizzbuzz(2, "2")


def test_return_fizz_with_3_passed_in():
    check_fizzbuzz(3, "Fizz")


def test_return_buzz_with_5_passed_in():
    check_fizzbuzz(5, "Buzz")


def test_return_fizz_with_6():
    check_fizzbuzz(6, "Fizz")


def test_return_fizz_with_10():
    check_fizzbuzz(10, "Buzz")


def test_return_fizzbuzz_with_15():
    check_fizzbuzz(15, "FizzBuzz")
