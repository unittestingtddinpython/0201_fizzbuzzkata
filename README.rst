FizzBuzz
========

Use Cases
---------

* Can I call FizzBuzz
* Get "1" when I pass in 1
* Get "2" when I pass in 2
* Get "Fizz" when I pass in 3
* Get "Buzz" when I pass in 5
* Get "Fizz" when I pass in 6 (a multiple of 3)
* Get "Buzz" when I pass in 10 (a multiple of 5)
* Get "FizzBuzz" when I pass in 15 (a multiple of 3 and 5)

Preparing the Development
-------------------------

1. Ensure ``pyenv``, ``pipenv``, Python Version 3.7.1 are installed
2. Clone repository: ``<repo url>``
3. Fetch development dependencies ``make install``
4. Activate virtualenv: ``pipenv shell``

Usage
-----

Pass in a full database URL, the storage driver, and the destination.

Example 01:

::

        $ <cmd>

Example 02:

::

        $ <cmd>

Running Tests
-------------

Run tests locally using ``make`` if virtualenv is active:

::

        $ make

If virtualenv is not active then use:

::

        $ pipenv run make


Author
------

*<Your Name>*
  - Initial work*
  - [<repo host> i.e. Gitlab, Github](<repo url> )


License
-------

::

        Copyright (C) <Company> - All Rights Reserved
        Unauthorized copying of this file, via any medium is strictly prohibited
        Proprietary and confidential
        Written by <author> <author@example.com>, Jan YYYY

